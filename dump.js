var fs = require('fs');

module.exports = {
    'update': function (data)
    {
        var file = './cache/dump.json';

        fs.writeFileSync(
            file,
            JSON.stringify(data)
        );

        return data;
    },

    'get': function (data)
    {
        var file = './cache/dump.json';
        var res = null;

        try
        {
            res = JSON.parse(
                fs.readFileSync(file)
            );

            return res;
        }
        catch (e) {
            fs.writeFileSync(
                file,
                JSON.stringify(data)
            );
        }

        return data;
    }
};