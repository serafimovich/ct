```bash
$ npm install
$ npm start &
```
Скрипт будет сохранять состояние счетчиков во время работы.

Для того, чтобы вернуться к значениям по умолчанию (или указанным в конфиге) - остановить сервис, удалить файл `cache/dump.json`, запустить сервис.

```bash
$ npm stop
$ rm -f cache/dump.json
$ npm start &
```

### Настройка

Файл конфига `config/config.json` по умолчанию:
```json
{
    "origins": null,
    "counters": {
        "tickets": {
            "delay": 1000,
            "value": 2,
            "start": 0
        },
        "orgs": {
            "delay": 86400000,
            "value": 3,
            "start": 0
        },
        "resellers": {
            "delay": 259200000,
            "value": 1,
            "start": 0
        },
        "cashflow": {
            "delay": 1000,
            "value": 0.6,
            "start": 0
        }
    }
}
```

Формат конфига счетчика:

```json
{
    "delay": 1000, /* задержка обновления в мс */
    "value": 2,    /* значение на которое изменится счетчик */
    "start": 0     /* начальное значение */
}
```

Чтобы ограничить доступ к сервису только текущему хосту, нужно указать `origins` в следующем формате:
```json
{
    "origins": ["yourdomain.com:*"]
}
```

### Пример клиента
Пример есть в файле `sample.html`
```html
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
        <script>
            $(function ()
            {
                var socket = io.connect('http://localhost:3000');
                var counters = ['tickets', 'orgs', 'resellers', 'cashflow'];

                $.each(
                    counters,
                    function (key, counter)
                    {
                        socket.on(counter, function (data) {
                            $('#' + counter).html(data);
                        });
                    }
                );
            });
        </script>
```