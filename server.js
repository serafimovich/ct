process.title = 'counter-ws-server';

var config = require('./config/config.json');
var io = require('socket.io')(3000);
var dump = require('./dump');

if (config.origins)
    io.set('origins', config.origins);

var counters = dump.get(config.counters);

var cb = function (counter)
{
    counters[counter]['start'] = parseFloat((counters[counter]['start'] + counters[counter]['value']).toFixed(1));
    io.sockets.emit(counter, counters[counter]['start']);
};

for (var counter in counters)
{
    setInterval(cb.bind(null, counter), config.counters[counter]['delay']);
    console.log('Counter ' + counter + ' started');
}

setInterval(dump.update.bind(null, counters), 5000);

io.on(
    'connection',
    function (socket)
    {
        for (var counter in counters)
            socket.emit(counter, parseFloat(counters[counter]['start'].toFixed(1)));
    }
);